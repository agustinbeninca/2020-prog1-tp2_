﻿using System;
using Logica;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP2_Beninca_Chapo
{
    public partial class Aplicacion : Form
    {
        Principal principal = new Principal();
        public Aplicacion()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void grillaEmpleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        { 

        }
        private void btnBusqueda_Click(object sender, EventArgs e)
        {
            string dato = textBox1.Text;
            principal.RegistrarAsistencia(dato);
            DateTime desde = new DateTime(2020, 10, 20);
            List<InformeEmpleado> ListaRegistro = new List<InformeEmpleado>();
            ListaRegistro = principal.GenerarInformeIndividual(desde, DateTime.Now,dato);
            grillaEmpleados.AutoGenerateColumns = true;
            grillaEmpleados.DataSource = null;
            grillaEmpleados.DataSource = ListaRegistro;
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (!textBox1.Text.Trim().Equals(""))
            {
                List<string> Nombres = principal.ObtenerNombres().Where(d => d.Contains(textBox1.Text.Trim())).ToList();
                ActualizarListBox(Nombres);
                //principal.ObtenerNombres().Where(d => d.Contains(textBox1.Text.Trim())).ToList();
            }
            else
                listBoxEmpleados.DataSource = principal.ObtenerNombres();
        }

        private void ActualizarListBox(List<string> nombres)
        {
            listBoxEmpleados.DataSource = null;
            listBoxEmpleados.DataSource = nombres;
            listBoxEmpleados.DisplayMember = "NombreApellido";
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = listBoxEmpleados.Text;
        }

        
    }
}