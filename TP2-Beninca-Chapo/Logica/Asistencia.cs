﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Asistencia
    {
        public DateTime FechaHoraIngreso { get; set; }
        public DateTime FechaHoraSalida { get; set; }
        public int LegajoDeEmpleado { get; set; }
    }
}
