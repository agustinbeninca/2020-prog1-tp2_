﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        public List<Empleado> Empleados { get; set; }
        public List<Turno> Turnos { get; set; }
        public List<Asistencia> Asistencias { get; set; }
        public List<InformeEmpleado> Informes { get; set; }


        public List<string> ObtenerNombres()
        {
            List<string> nombres = new List<string>();
            foreach (var item in Empleados)
            {
                nombres.Add(item.NombreCompleto);
            }
            return nombres;
        }

        //empleados
        public void CargarEmpleados(string nombreApellido, string domicilio, string localidad, string telefono, string correoElectronico, double sueldoNeto, int numeroTurno, int estado)
        {
            Empleado nuevoEmpleado = new Empleado();
            nuevoEmpleado.Legajo = Empleados.Count + 1;
            nuevoEmpleado.NombreCompleto = nombreApellido;
            nuevoEmpleado.Domicilio = domicilio;
            nuevoEmpleado.Localidad = localidad;
            nuevoEmpleado.Telefono = telefono;
            nuevoEmpleado.CorreoElectronico = correoElectronico;
            nuevoEmpleado.SueldoNeto = sueldoNeto;
            nuevoEmpleado.Turno = ObtenerTurno(numeroTurno);
            nuevoEmpleado.EstadoEmpleado = estado;
            Empleados.Add(nuevoEmpleado);
        }
        private Empleado ObtenerEmpleado(int legajo)
        {
            Empleado empleadoEncontrado = new Empleado();
            empleadoEncontrado = Empleados.First(x => x.Legajo == legajo);
            return empleadoEncontrado;
        }
        public void ModificarEmpleado(int legajo, string nombreApellido, string domicilio, string localidad, string telefono, string correoElectronico, double sueldoNeto, int numeroTurno)
        {
            Empleado empleado = ObtenerEmpleado(legajo);
            empleado.Legajo = legajo;
            empleado.NombreCompleto = nombreApellido;
            empleado.Domicilio = domicilio;
            empleado.Localidad = localidad;
            empleado.Telefono = telefono;
            empleado.CorreoElectronico = correoElectronico;
            empleado.SueldoNeto = sueldoNeto;
            empleado.Turno = ObtenerTurno(numeroTurno);
        }
        private Turno ObtenerTurno(int dato)
        {
            return Turnos.First(x => x.Numero == dato);
        }
        public void EliminarEmpleado(int legajo)
        {
            bool asistencia = VerificarAsistencia(legajo);
            if (asistencia == true)
            {
                Empleados.First(x => x.Legajo == legajo).EstadoEmpleado = 99;
            }
        }
        public bool VerificarAsistencia(int legajo)
        {
            foreach (var item in Asistencias)
            {
                if (item.FechaHoraIngreso.Date == DateTime.Today.Date)
                {
                    if (item.LegajoDeEmpleado == legajo)
                    {
                        if (item.FechaHoraSalida != null)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
       

        //turnos
        public void CargarTurno(int numero, string descripcion, List<int> dias, int horaDesde, int horaHasta, int estado)
        {
            Turno nuevoTurno = new Turno();
            nuevoTurno.Numero = numero;
            nuevoTurno.Descripcion = descripcion;
            nuevoTurno.Dias = dias;
            nuevoTurno.HoraDesde = horaDesde;
            nuevoTurno.HoraHasta = horaHasta;
            nuevoTurno.EstadoTurno = estado;
        }
        public string BuscarTurno(string descripcion, int dia)
        {
            Turno turno = Turnos.First(x => x.Descripcion == descripcion);
            foreach (var item in turno.Dias)
            {
                if (item == dia)
                {
                    return "Turno:  " + turno.Numero + "Desde: " + turno.HoraDesde + "Hasta: " + turno.HoraHasta + "Día: " + dia;
                }
            }
            return "No se encontraron coincidencias.";
        }
        public void ModificarTurno(int numero, string descripcion, List<int> dias, int horaDesde, int horaHasta)
        {
            Turno turno = ObtenerTurno(numero);
            turno.Numero = numero;
            turno.Descripcion = descripcion;
            turno.Dias = dias;
            turno.HoraDesde = horaDesde;
            turno.HoraHasta = horaHasta;
        }
        public bool EliminarTurno(int numero)
        {
            int cont = 0;
            foreach (var item in Empleados)
            {
                if (item.Turno.Numero == numero)
                {
                    cont++;
                }
            }
            if (cont == 0)
            {
                Turnos.First(x => x.Numero == numero).EstadoTurno = 99;
                return true;
            }
            else
            {
                return false;
            }
        }

        //asistencias
        public void RegistrarAsistencia(string nombre)
        {
            Empleado empleadoEncontrado = Empleados.First(x => x.NombreCompleto == nombre);
            int legajo = empleadoEncontrado.Legajo;
            bool bandera = true;

            foreach (var item in Asistencias)
            {
                if (item.FechaHoraIngreso.Date == DateTime.Today.Date)
                {
                    if (item.LegajoDeEmpleado == legajo)
                    {
                        item.FechaHoraSalida = DateTime.Now;
                        bandera = false;
                        GuardarArchivo3();
                    }
                }
            }
            if (bandera==true)
            {
                Asistencia asistencia = new Asistencia();
                asistencia.FechaHoraIngreso = DateTime.Now;
                asistencia.LegajoDeEmpleado = legajo;
                Asistencias.Add(asistencia);
                GuardarArchivo3();
            }
        }
        public int CalcularTardanzas(DateTime fechaDesde, DateTime fechaHasta, int legajoEmpleado)
        {
            Empleado empleado = ObtenerEmpleado(legajoEmpleado);
            DateTime diaActual = fechaDesde;
            int tardanzas = 0;
            while (diaActual >= fechaDesde && diaActual <= fechaHasta)
            {
                if (empleado.DebeTrabajar(diaActual) == true)
                {
                    foreach (var item in Asistencias)
                    {
                        if (item.FechaHoraIngreso.Date == diaActual.Date)
                        {
                            DateTime horaIngreso = diaActual.AddHours(empleado.Turno.HoraDesde);
                            TimeSpan diferenciaHoraria = (horaIngreso - item.FechaHoraIngreso);
                            double minutosDiferencia = diferenciaHoraria.TotalMinutes;
                            if (minutosDiferencia < -15)
                            {
                                tardanzas++;
                            }
                        }
                    }
                }
                diaActual.AddDays(1);
            }
            return tardanzas;
        }
        public int CalcularInasistencia(DateTime fechaDesde, DateTime fechaHasta, int legajoEmpleado)
        {
            Empleado empleado = ObtenerEmpleado(legajoEmpleado);
            DateTime diaActual = fechaDesde;
            int inasistencias = 0;
            while (diaActual >= fechaDesde && diaActual <= fechaHasta)
            {
                if (empleado.DebeTrabajar(diaActual) == true)
                {
                    foreach (var item in Asistencias)
                    {
                        if (item.LegajoDeEmpleado != legajoEmpleado)
                        {
                            inasistencias++;
                        }
                    }
                }
                diaActual.AddDays(1);
            }
            return inasistencias;
        }
        public int CalcularHorasExtra(DateTime fechaDesde, DateTime fechaHasta, int legajoEmpleado)
        {
            Empleado empleado = ObtenerEmpleado(legajoEmpleado);
            DateTime diaActual = fechaDesde;
            int horasExtra = 0;
            while (diaActual >= fechaDesde && diaActual <= fechaHasta)
            {
                if (empleado.DebeTrabajar(diaActual) == true)
                {
                    foreach (var item in Asistencias)
                    {
                        if (item.FechaHoraIngreso.Date == diaActual.Date)
                        {
                            DateTime horaIngreso = diaActual.AddHours(empleado.Turno.HoraDesde);
                            TimeSpan diferenciaIngreso = (horaIngreso - item.FechaHoraIngreso);
                            double minutosIngreso = diferenciaIngreso.TotalMinutes;
                            DateTime horaSalida = diaActual.AddHours(empleado.Turno.HoraHasta);
                            TimeSpan diferenciaSalida = (horaSalida - item.FechaHoraSalida);
                            double minutosSalida = diferenciaSalida.TotalMinutes;
                            if ((minutosIngreso + minutosSalida) >= 30)
                            {
                                horasExtra++;
                            }
                        }
                    }
                }
                diaActual.AddDays(1);
            }
            return horasExtra;
        }

        //informes
        public List<InformeEmpleado> GenerarInformeIndividual(DateTime fechaDesde, DateTime fechaHasta, string nombre)
        {
            List<InformeEmpleado> informe = new List<InformeEmpleado>();
            Empleado empleadoEncontrado = Empleados.First(x => x.NombreCompleto == nombre);
            int legajoEmpleado = empleadoEncontrado.Legajo;
            foreach (var item in Asistencias)
            {
                if (item.FechaHoraIngreso.Date >= fechaDesde.Date && item.FechaHoraSalida.Date <= fechaHasta.Date)
                {
                    if (item.LegajoDeEmpleado == legajoEmpleado)
                    {
                        InformeEmpleado nuevoInforme = new InformeEmpleado();
                        nuevoInforme.LegajoEmpleado = item.LegajoDeEmpleado;
                        nuevoInforme.FechaYHoraEntrada = item.FechaHoraIngreso;
                        nuevoInforme.FechaYHoraSalida = item.FechaHoraSalida;
                        nuevoInforme.Turno = empleadoEncontrado.Turno.Numero;
                        informe.Add(nuevoInforme);
                    }
                }
            }
            return informe;
        }
        public List<InformeTotal> GenerarInformeTotal(DateTime fechaDesde, DateTime fechaHasta)
        {
            List<InformeTotal> informeGenerado = new List<InformeTotal>();
            DateTime fecha = fechaDesde;
            foreach (var item in Empleados)
            {
                InformeTotal nuevoInforme = new InformeTotal();
                nuevoInforme.Legajo = item.Legajo;
                nuevoInforme.NombreEmpleado = item.NombreCompleto;
                nuevoInforme.DiasTrabajados = Asistencias.Count(x => x.LegajoDeEmpleado == item.Legajo);
                nuevoInforme.DiasAusente = CalcularInasistencia(fechaDesde, fechaHasta, item.Legajo);
                nuevoInforme.HorasExtra = CalcularHorasExtra(fechaDesde, fechaHasta, item.Legajo);
                nuevoInforme.Tardanzas = CalcularTardanzas(fechaDesde, fechaHasta, item.Legajo);
                informeGenerado.Add(nuevoInforme);
            }
            return informeGenerado;
        }


        //JSON
        public Principal()
        {
            CrearArchivos();

            Empleados = LeerArchivo1();
            Turnos = LeerArchivo2();
            Asistencias = LeerArchivo3();
            Informes = LeerArchivo4();
        }

        public void CrearArchivos()
        {
            if (!File.Exists(@"C:\Users\Agustín\Documents\Archivos Json\ListaEmpleados.txt"))
            {
                File.Create(@"C:\Users\Agustín\Documents\Archivos Json\ListaEmpleados.txt").Close();
            }
            if (!File.Exists(@"C:\Users\Agustín\Documents\Archivos Json\ListaTurnos.txt"))
            {
                File.Create(@"C:\Users\Agustín\Documents\Archivos Json\ListaTurnos.txt").Close();
            }
            if (!File.Exists(@"C:\Users\Agustín\Documents\Archivos Json\ListaAsistencias.txt"))
            {
                File.Create(@"C:\Users\Agustín\Documents\Archivos Json\ListaAsistencias.txt").Close();
            }
            if (!File.Exists(@"C:\Users\Agustín\Documents\Archivos Json\ListaInformes.txt"))
            {
                File.Create(@"C:\Users\Agustín\Documents\Archivos Json\ListaInformes.txt").Close();
            }
        }

        //lista empleados
        public bool GuardarArchivo1()
        {
            string ubicacion = @"C:\Users\Agustín\Documents\Archivos Json\ListaEmpleados.txt";
            using (StreamWriter escritor = new StreamWriter(ubicacion, false))
            {
                string datos = JsonConvert.SerializeObject(Empleados);
                escritor.Write(datos);
                return true;
            }
        }
        public List<Empleado> LeerArchivo1()
        {
            string ubicacion = @"C:\Users\Agustín\Documents\Archivos Json\ListaEmpleados.txt";
            using (StreamReader lector = new StreamReader(ubicacion))
            {
                List<Empleado> Empleados = new List<Empleado>();
                string datos = lector.ReadToEnd();
                if (datos != "")
                {
                    Empleados = JsonConvert.DeserializeObject<List<Empleado>>(datos);
                }
                return Empleados;
            }
        }

        //lista turnos
        public bool GuardarArchivo2()
        {
            string ubicacion = @"C:\Users\Agustín\Documents\Archivos Json\ListaTurnos.txt";
            using (StreamWriter escritor = new StreamWriter(ubicacion, false))
            {
                string datos = JsonConvert.SerializeObject(Turnos);
                escritor.Write(datos);
                return true;
            }
        }
        public List<Turno> LeerArchivo2()
        {
            string ubicacion = @"C:\Users\Agustín\Documents\Archivos Json\ListaTurnos.txt";
            using (StreamReader lector = new StreamReader(ubicacion))
            {
                List<Turno> Turnos = new List<Turno>();
                string datos = lector.ReadToEnd();
                if (datos != "")
                {
                    Turnos = JsonConvert.DeserializeObject<List<Turno>>(datos);
                }
                return Turnos;
            }
        }

        //lista asistencias
        public  bool GuardarArchivo3()
        {
            string ubicacion = @"C:\Users\Agustín\Documents\Archivos Json\ListaAsistencias.txt";
            using (StreamWriter escritor = new StreamWriter(ubicacion, false))
            {
                string datos = JsonConvert.SerializeObject(Asistencias);
                escritor.Write(datos);
                return true;
            }
        }
        public List<Asistencia> LeerArchivo3()
        {
            string ubicacion = @"C:\Users\Agustín\Documents\Archivos Json\ListaAsistencias.txt";
            using (StreamReader lector = new StreamReader(ubicacion))
            {
                List<Asistencia> Asistencias = new List<Asistencia>();
                string datos = lector.ReadToEnd();
                if (datos != "")
                {
                    Asistencias = JsonConvert.DeserializeObject<List<Asistencia>>(datos);
                }
                return Asistencias;
            }
        }

        //lista informes
        public bool GuardarArchivo4()
        {
            string ubicacion = @"C:\Users\Agustín\Documents\Archivos Json\ListaInformes.txt";
            using (StreamWriter escritor = new StreamWriter(ubicacion, false))
            {
                string datos = JsonConvert.SerializeObject(Informes);
                escritor.Write(datos);
                return true;
            }
        }
        public List<InformeEmpleado> LeerArchivo4()
        {
            string ubicacion = @"C:\Users\Agustín\Documents\Archivos Json\ListaInformes.txt";
            using (StreamReader lector = new StreamReader(ubicacion))
            {
                List<InformeEmpleado> Informes = new List<InformeEmpleado>();
                string datos = lector.ReadToEnd();
                if (datos != "")
                {
                    Informes = JsonConvert.DeserializeObject<List<InformeEmpleado>>(datos);
                }
                return Informes;
            }
        }
    }
}
