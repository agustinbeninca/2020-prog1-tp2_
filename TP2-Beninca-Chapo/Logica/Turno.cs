﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Turno
    {
        public int Numero { get; set; }
        public string Descripcion { get; set; }
        public List<int> Dias { get; set; }
        public int HoraDesde { get; set; }
        public int HoraHasta { get; set; }
        public int EstadoTurno { get; set; } //0=activo - 99=eliminado

    }
}
