﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class InformeTotal
    {
        public int Legajo { get; set; }
        public string NombreEmpleado { get; set; }
        public int DiasTrabajados { get; set; }
        public int DiasAusente { get; set; }
        public int HorasExtra { get; set; }
        public int Tardanzas { get; set; }
    }
}
