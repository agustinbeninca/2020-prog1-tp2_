﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Empleado
    {
        public int Legajo { get; set; }
        public string NombreCompleto { get; set; }
        public string Domicilio { get; set; }
        public string Localidad { get; set; }
        public string Telefono { get; set; }
        public string CorreoElectronico { get; set; }
        public double SueldoNeto { get; set; }
        public Turno Turno { get; set; }
        public int EstadoEmpleado { get; set; } //0=activo - 99=eliminado

        public bool DebeTrabajar(DateTime diaActual)
        {
            int numeroDia = Convert.ToInt32(diaActual.DayOfWeek);
            foreach (var item in Turno.Dias)
            {
                if (item==numeroDia)
                {
                    return true;
                }
            }
            return false;
        }
        
    }
}
