﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class InformeEmpleado
    {
        public int Turno { get; set; }
        public DateTime FechaYHoraEntrada { get; set; }
        public DateTime FechaYHoraSalida { get; set; }
        public int LegajoEmpleado { get; set; }

    }
}
